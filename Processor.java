package com.cdstms.cds.core.batch.test.arun;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

/**
 * @author sudeep.shakya
 * @created 09/06/2021
 * @project cdstmsbatch
 */
@Slf4j
public class Processor implements ItemProcessor<String,OCRPrepFilesEntity>, StepExecutionListener {

    String mountedPath;
    String createdResFilePath;

    @Override
    public void beforeStep(StepExecution stepExecution) {

        mountedPath = stepExecution.getJobParameters().getString("newMountedFilePath");
        createdResFilePath = stepExecution.getJobParameters().getString("createdPath");
        log.info("Inside FileProcessor fullFilePath=>" +mountedPath );
        log.info("ResFilePath for creating resfiles and retrieving record via createdResFilePath=>" +createdResFilePath);


        log.info("Processor before step.");
    }


    @Override
    public OCRPrepFilesEntity process(String file) {

        log.info("Inside processor.");

        OCRPrepJobEntity ocrPrepJobEntityType = ocrPrepJobRepository.findByFolderPath(createdResFilePath);

        OCRPrepFilesEntity ocrPrepFilesEntity = new OCRPrepFilesEntity();
        ocrPrepFilesEntity.setFileName(file.substring(0, file.lastIndexOf('.')));
        String[] splittedFileName = file.split("_");
        ocrPrepFilesEntity.setLoanNumber(splittedFileName[0]);
        ocrPrepFilesEntity.setJobFolderId(ocrPrepJobEntityType);

        Path path= Paths.get(mountedPath+ File.separator+ file);
        log.info("======> current file : " + path);
        String loanNumber = splittedFileName[0];


        Map<String, Object> currentDocProperties = new HashMap<>();
        currentDocProperties.put("OriginalFileName", path.getFileName());
        currentDocProperties.put("EntryDate", "");

        Map<String, Map<String, Object>> docLevelProperties = new HashMap<>();
        docLevelProperties.put(String.valueOf(path), currentDocProperties);

        Map<String, Object> batchLevelProperties = new HashMap<>();
        batchLevelProperties.put("LoanNumber", loanNumber);
        ResFileUtility.generateResFile(mountedPath, createdResFilePath,batchLevelProperties, docLevelProperties, FilenameUtils.removeExtension(path.getFileName().toString()));

        //      return new Student(item.getEmailAddress(),item.getName());

        return ocrPrepFilesEntity;
    }

    @Override
    public ExitStatus afterStep(StepExecution stepExecution) {
        log.info("Processor after step.");
        return ExitStatus.COMPLETED;
    }
}

