package com.cdstms.cds.core.batch.test.arun;

import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemStream;
import org.springframework.batch.item.ItemStreamException;

import java.io.IOException;
import java.nio.file.FileSystemException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author sudeep.shakya
 * @created 09/06/2021
 * @project cdstmsbatch
 */
@Slf4j
@NoArgsConstructor
public class Reader implements ItemReader<String>, StepExecutionListener, ItemStream {

    private List<String> fileList;

    int currentIndex = 0;

    private static final String CURRENT_INDEX = "current.index";

    private AtomicInteger current = new AtomicInteger(0);

    public Reader(String fileName) throws IOException {

        log.info("Inside constructor => " + fileName);
        initArun(fileName);

    }

    private void initArun(String filePath) throws IOException {

        Path start = Paths.get(filePath);

        log.info("Starting directory for Listing Images =>" + start);

        try (Stream<Path> stream = Files.walk(start, 1)) {

            fileList = stream.filter(s -> !Files.isDirectory(s))
                    .map(s -> String.valueOf(s.getFileName().toString())).sorted().collect(Collectors.toList());
            fileList.forEach(System.out::println);
        } catch (FileSystemException e) {
            //     throw new EcmOCRPrepException("Invalid file path.");
        }

    }

    @Override
    public synchronized String read() {
        int index = this.current.getAndIncrement();
        if (index < fileList.size()) {
            return fileList.get(index);
        } else
            return null;
    }

    @Override
    public void beforeStep(StepExecution stepExecution) {

    }

    @Override
    public ExitStatus afterStep(StepExecution stepExecution) {
        return null;
    }

    @Override
    public void open(ExecutionContext executionContext) throws ItemStreamException {
        if (executionContext.containsKey(CURRENT_INDEX)) {
            currentIndex = new Long(executionContext.getLong(CURRENT_INDEX)).intValue();
        } else {
            currentIndex = 0;
        }
    }

    @Override
    public void update(ExecutionContext executionContext) throws ItemStreamException {
        executionContext.putLong(CURRENT_INDEX, new Long(currentIndex).longValue());
    }

    @Override
    public void close() throws ItemStreamException {

    }
}
