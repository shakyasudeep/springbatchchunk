package com.cdstms.cds.core.batch.test.arun;

import com.cdstms.cds.core.batch.listener.JobCompletionNotificationListener;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.integration.async.AsyncItemProcessor;
import org.springframework.batch.integration.async.AsyncItemWriter;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.io.IOException;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * @author sudeep.shakya
 * @created 09/06/2021
 * @project cdstmsbatch
 */
@Configuration
public class JobConfig {

    @Autowired
    private JobBuilderFactory jobBuilderFactory;

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Bean
    public TaskExecutor taskExecutor1() {

        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(64);
        executor.setMaxPoolSize(100);
        executor.setQueueCapacity(100);
        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        executor.setThreadNamePrefix("Chunk-");
        return executor;

    }

    @Bean(name = "testReader1")
    @StepScope
    public Reader reader(@Value("#{jobParameters[filePath]}") String filePath) throws IOException {
        return new Reader(filePath);
        //   return new Reader();
    }

    @Bean(name = "testProcessor")
    public ItemProcessor processor() {
        return new Processor();
    }

    @Bean
    public AsyncItemProcessor<String, OCRPrepFilesEntity> asyncProcessor1() {

        AsyncItemProcessor<String, OCRPrepFilesEntity> asyncItemProcessor = new AsyncItemProcessor<>();
        asyncItemProcessor.setDelegate(processor());
        asyncItemProcessor.setTaskExecutor(taskExecutor1());

        return asyncItemProcessor;
    }

    @Bean(name = "testWriter")
    public ItemWriter writer() {
        return new Writer();
    }

    @Bean
    public AsyncItemWriter<OCRPrepFilesEntity> asyncWriter1() {

        AsyncItemWriter<OCRPrepFilesEntity> asyncItemWriter = new AsyncItemWriter<>();
        asyncItemWriter.setDelegate(writer());
        return asyncItemWriter;

    }

    @Bean
    public Step step1(@Qualifier("testReader1") ItemReader<String> importReader) {
        return stepBuilderFactory.get("step1").<String, Future<OCRPrepFilesEntity>>chunk(4)
                .reader(importReader)
                .processor(asyncProcessor1())
                .writer(asyncWriter1())
                //			.listener(stepListener())
                .taskExecutor(taskExecutor1())
                .build();
    }

    @Bean(name = "testJob")
    public Job testUserJob(ItemReader<String> importReader, JobCompletionNotificationListener listener) {
        return jobBuilderFactory
                .get("testUserJob")
                .incrementer(new RunIdIncrementer())
                .listener(listener)
                .flow(step1(importReader))
                .end()
                .build();
    }

}
