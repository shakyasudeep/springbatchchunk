package com.cdstms.cds.core.batch.test.arun;

import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;

/**
 * @author sudeep.shakya
 * @created 09/06/2021
 * @project cdstmsbatch
 */
@Slf4j
public class Writer implements ItemWriter<OCRPrepFilesEntity>, StepExecutionListener {

    @Autowired
    OCRPrepFilesRepo ocrPrepFilesRepo;

    @Autowired
    OCRBatchRepo ocrBatchRepo;

    @Autowired
    OCRApiClientService ocrApiService;

    private static final String FILE_SEPARATOR = "\\";

    String fullFilePath;
    String processName;
    String priority;
    String retryFolderPath;
    String vendorName;


    @Override
    public void beforeStep(StepExecution stepExecution) {

        fullFilePath = stepExecution.getJobParameters().getString("createdPath");
        processName = stepExecution.getJobParameters().getString("processName");
        priority = stepExecution.getJobParameters().getString("priority");
        retryFolderPath = stepExecution.getJobParameters().getString("retryFolder");
        vendorName = stepExecution.getJobParameters().getString("vendorName");
        log.info("Inside File Writer for sending to paradatec processName =>" + processName + " and priority =>" + priority + " and fullfilepath =>" + fullFilePath);

    }

    @Override
    public void write(List<? extends OCRPrepFilesEntity> fileList) throws Exception {

        ocrPrepFilesRepo.saveAll(fileList);

        List<OCRBatchEntity> ocrBatchEntities = new ArrayList<>();

        for (OCRPrepFilesEntity ocrPrepFilesEntity : fileList) {

            File resFile = new File(fullFilePath + FILE_SEPARATOR + ocrPrepFilesEntity.getFileName() + ".res");

            try {
                String batchId = ocrApiService.processFilesByOCR(resFile.getPath(), processName, priority);
                log.info("batchId => " + batchId);
                OCRBatchEntity ocrPrepBatchEntity = new OCRBatchEntity();
                ocrPrepBatchEntity.setBatchId(batchId);
                ocrPrepBatchEntity.setStatus("CREATED");
                ocrPrepBatchEntity.setFolderPath(fullFilePath);
                ocrPrepBatchEntity.setVendorName(vendorName);
                ocrBatchEntities.add(ocrPrepBatchEntity);

                ocrPrepFilesEntity.setBatchId(batchId);
                ocrPrepFilesEntity.setStatus(FileStatus.SUBMITTED);
                ocrPrepFilesEntity.setFolderPath(fullFilePath);
            } catch (Exception ex) {
                log.info("Error occured while attempting to create batch id.");
                log.info("Batch id will be returned null due to : " + ex.getMessage());
                String batchId = String.valueOf((Object) null);

                ocrPrepFilesEntity.setBatchId(batchId);
                ocrPrepFilesEntity.setStatus(FileStatus.FAILURE);
                ocrPrepFilesEntity.setFolderPath(fullFilePath);
                ocrPrepFilesEntity.setErrorInfo(ex.getMessage());
                Date errorDate = new Date();
                ocrPrepFilesEntity.setErrorDate(errorDate);

                File retryFolder = new File(retryFolderPath + FILE_SEPARATOR + "ApiProcessingError");
                File retryFile = new File(retryFolder + FILE_SEPARATOR + ocrPrepFilesEntity.getFileName() + ".pdf");
                File errorFile = new File(fullFilePath + FILE_SEPARATOR + ocrPrepFilesEntity.getFileName() + ".pdf");

                if (!retryFolder.exists()) {
                    Files.createDirectory(retryFolder.toPath());
                    log.info("Moving file to ApiProcessingError Folder in Retry location.");
                    Files.move(errorFile.toPath(), retryFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
                    Files.delete(resFile.toPath());
                } else {
                    log.info("Moving file to ApiProcessingError Folder in Retry location.");
                    Files.move(errorFile.toPath(), retryFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
                    Files.delete(resFile.toPath());
                }
            }

            String resPath = fullFilePath + FILE_SEPARATOR + ocrPrepFilesEntity.getFileName() + ".res";
            log.info("Invoking OCR web service for processing resFiles with resPath=>" + resPath);

        }

        ocrBatchRepo.saveAll(ocrBatchEntities);
        ocrPrepFilesRepo.saveAll(fileList);

    }

    @Override
    public ExitStatus afterStep(StepExecution stepExecution) {
        return null;
    }
}
